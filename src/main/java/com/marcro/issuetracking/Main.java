package com.marcro.issuetracking;

import com.marcro.issuetracking.datetime.DateTime;
import com.marcro.issuetracking.datetime.Time;
import com.marcro.issuetracking.duedate.DateTimeValidator;
import com.marcro.issuetracking.duedate.DueDateCalculatorImpl;
import com.marcro.issuetracking.duration.TimeUnit;
import com.marcro.issuetracking.duration.WorkDuration;
import com.marcro.issuetracking.exception.NotWorkingDayException;
import com.marcro.issuetracking.exception.OutsideWorkingHoursException;


import static com.marcro.issuetracking.datetime.Time.AM;
import static com.marcro.issuetracking.datetime.Time.PM;

public class Main {
    public static void main(String[] args) throws OutsideWorkingHoursException, NotWorkingDayException {
        DateTimeValidator dateTimeValidator = new DateTimeValidator(new Time(9, 0, AM), new Time(5,0, PM));
        DueDateCalculatorImpl dueDateCalculator = new DueDateCalculatorImpl(dateTimeValidator);

        DateTime submitTime = new DateTime(2023, 8, 7, 9, 0, AM);
        WorkDuration turnaroundTime = new WorkDuration(3, TimeUnit.DAYS);

        System.out.println(dueDateCalculator.calculateDueDate(submitTime, turnaroundTime));
    }
}