package com.marcro.issuetracking.exception;

public class NotWorkingDayException extends Exception {
    public NotWorkingDayException(String message) {
        super(message);
    }
}
