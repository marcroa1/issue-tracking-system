package com.marcro.issuetracking.exception;

public class OutsideWorkingHoursException extends Exception {
    public OutsideWorkingHoursException(String message) {
        super(message);
    }
}
