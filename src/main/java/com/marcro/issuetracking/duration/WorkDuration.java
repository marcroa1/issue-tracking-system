package com.marcro.issuetracking.duration;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.marcro.issuetracking.duration.TimeUnit.*;

public class WorkDuration {
    public static final int MINUTES_SCALE = 60;
    public static final int WORK_HOURS_SCALE = 8;

    private final Map<TimeUnit, TimeHolder> timeHolders = new LinkedHashMap<>();

    public WorkDuration(long amount, TimeUnit timeUnit) {
        initTimeHolders();
        extend(amount, timeUnit);
    }

    private void initTimeHolders() {
        Stream.of(TimeUnit.values()).forEach(timeUnit ->
                timeHolders.put(timeUnit, new TimeHolder(0, timeUnit))
        );
    }

    /**
     * Gets a specific time unit, and returns it as a {@link TimeHolder}
     */
    public TimeHolder get(TimeUnit timeUnit) {
        return timeHolders.get(timeUnit);
    }

    /**
     * Gets the duration as a List of {@link TimeHolder}.
     */
    public Collection<TimeHolder> getTimeHolders() {
        return timeHolders.values();
    }


    /**
     * Extends the current duration with the given time
     * If a given time unit exceeds its scale, then it is automatically converted to the next time unit.
     * E.g.: 70 minutes will be 1 hour and 10 minutes
     * @param duration  The amount of time units
     * @param timeUnit  The time unit to be extended
     */
    public void extend(long duration, TimeUnit timeUnit) {
        duration += currentDuration(timeUnit);

        switch (timeUnit) {

            case MINUTES -> {
                extend(getHours(duration), HOURS);
                add(getRemainingMinutes(duration), timeUnit);
            }

            case HOURS -> {
                extend(getDays(duration), DAYS);
                add(getRemainingHours(duration), timeUnit);
            }

            case DAYS -> add(duration, timeUnit);

            default -> throw new IllegalStateException("Unexpected value: " + timeUnit);
        }
    }

    private long currentDuration(TimeUnit timeUnit) {
        return get(timeUnit).amount();
    }

    private static long getDays(long duration) {
        return duration / WORK_HOURS_SCALE;
    }

    private static long getHours(long duration) {
        return duration / MINUTES_SCALE;
    }

    private static long getRemainingMinutes(long duration) {
        return duration % MINUTES_SCALE;
    }

    private static long getRemainingHours(long duration) {
        return duration % WORK_HOURS_SCALE;
    }

    private void add(Long value, TimeUnit key) {
        timeHolders.put(key, new TimeHolder(value, key));
    }

    @Override
    public String toString() {
        return timeHolders.values().stream()
                .filter(timeHolder -> timeHolder.amount() > 0)
                .map(TimeHolder::toString)
                .collect(Collectors.joining(", "));
    }
}
