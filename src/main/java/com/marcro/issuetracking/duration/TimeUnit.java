package com.marcro.issuetracking.duration;

public enum TimeUnit {
    DAYS,
    HOURS,
    MINUTES
}
