package com.marcro.issuetracking.duration;

/**
 * Pojo to hold time information.
 * @param amount    The amount of the given time unit
 * @param timeUnit  Unit of time in {@link TimeUnit}
 */
public record TimeHolder(long amount, TimeUnit timeUnit) {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimeHolder that = (TimeHolder) o;

        if (amount != that.amount) return false;
        return timeUnit == that.timeUnit;
    }

    @Override
    public int hashCode() {
        int result = (int) (amount ^ (amount >>> 32));
        result = 31 * result + (timeUnit != null ? timeUnit.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return amount + " " + timeUnit;
    }
}
