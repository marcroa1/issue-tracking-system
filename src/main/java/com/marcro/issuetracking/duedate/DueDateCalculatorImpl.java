package com.marcro.issuetracking.duedate;

import com.marcro.issuetracking.datetime.DateTime;
import com.marcro.issuetracking.datetime.Time;
import com.marcro.issuetracking.duration.TimeHolder;
import com.marcro.issuetracking.duration.TimeUnit;
import com.marcro.issuetracking.duration.WorkDuration;
import com.marcro.issuetracking.exception.NotWorkingDayException;
import com.marcro.issuetracking.exception.OutsideWorkingHoursException;

public class DueDateCalculatorImpl implements DueDateCalculator {
    private final DateTimeValidator dateTimeValidator;
    public DueDateCalculatorImpl(DateTimeValidator dateTimeValidator) {
        this.dateTimeValidator = dateTimeValidator;
    }

    /**
     * Calculates the due date from the given submit date time and the turnaround time.
     * @param submitDateTime                The submit date time as {@link DateTime}
     * @param turnaroundTime                The turnaround time, given in {@link WorkDuration}.
     * @return                              The due date as a {@link DateTime}
     * @throws NotWorkingDayException       If the submit date is on a weekend
     * @throws OutsideWorkingHoursException If the submit time is outside the working hours
     */
    @Override
    public DateTime calculateDueDate(DateTime submitDateTime, WorkDuration turnaroundTime) throws NotWorkingDayException, OutsideWorkingHoursException {
        DateTime timeToChange = DateTime.copy(submitDateTime);

        checkIfWorkingDay(timeToChange);
        checkIfWorkingTime(timeToChange);

        addDurationToDateTime(timeToChange, turnaroundTime);

        return timeToChange;
    }

    private void checkIfWorkingTime(DateTime submitDateTime) throws OutsideWorkingHoursException {
        if (dateTimeValidator.isOutsideWorkingHours(submitDateTime)) {
            Time wrongTime = submitDateTime.getTime();
            throw new OutsideWorkingHoursException(wrongTime + " is outside of working hours");
        }
    }

    private void checkIfWorkingDay(DateTime submitDateTime) throws NotWorkingDayException {
        if (dateTimeValidator.isWeekend(submitDateTime)) {
            String wrongDate = submitDateTime.getDateString();
            throw new NotWorkingDayException(wrongDate + " is not a working day");
        }
    }

    private void addDurationToDateTime(DateTime dateTime, WorkDuration duration) {
        for (TimeHolder timeHolder : duration.getTimeHolders()) {
            addTimeHolderToDateTime(dateTime, timeHolder);
        }
    }

    private void addTimeHolderToDateTime(DateTime dateTime, TimeHolder timeHolder) {
        for (int i = 0; i < timeHolder.amount(); i++) {
            addTimeUnitToDateTime(dateTime, timeHolder.timeUnit());
            skipNonWorkingHours(dateTime);
            skipWeekends(dateTime);
        }
    }

    private static void addTimeUnitToDateTime(DateTime dateTime, TimeUnit timeUnit) {
        switch (timeUnit) {
            case DAYS -> dateTime.incrementDay();
            case HOURS -> dateTime.addHours(1);
            case MINUTES -> dateTime.addMinutes(1);
        }
    }

    private void skipNonWorkingHours(DateTime submitDateTime) {
        while(dateTimeValidator.isOutsideWorkingHours(submitDateTime)) {
            submitDateTime.addHours(1);
        }
    }

    private void skipWeekends(DateTime submitDateTime) {
        while(dateTimeValidator.isWeekend(submitDateTime)) {
            submitDateTime.incrementDay();
        }
    }


}
