package com.marcro.issuetracking.duedate;

import com.marcro.issuetracking.duration.WorkDuration;
import com.marcro.issuetracking.datetime.DateTime;
import com.marcro.issuetracking.exception.NotWorkingDayException;
import com.marcro.issuetracking.exception.OutsideWorkingHoursException;

public interface DueDateCalculator {
    DateTime calculateDueDate(DateTime submitDateTime, WorkDuration turnaroundTime) throws NotWorkingDayException,OutsideWorkingHoursException;

}
