package com.marcro.issuetracking.duedate;

import com.marcro.issuetracking.datetime.DateTime;
import com.marcro.issuetracking.datetime.Time;

import java.util.List;

import static com.marcro.issuetracking.datetime.Days.SATURDAY;
import static com.marcro.issuetracking.datetime.Days.SUNDAY;

public class DateTimeValidator {
    public static final List<Integer> WEEKEND_DAYS = List.of(SATURDAY, SUNDAY);
    private final Time periodStart;
    private final Time periodEnd;
    public DateTimeValidator(Time periodStart, Time periodEnd) {
        this.periodStart = periodStart;
        this.periodEnd = periodEnd;
    }

    /**
     * Validates if the given time is between the PeriodStart and PeriodEnd time.
     * PeriodStart and PeriodEnd times are set on instantiation
     */
    public boolean isOutsideWorkingHours(Time time) {
        return time.isBefore(periodStart) || time.isAfter(periodEnd);
    }

    /**
     * Validates if the given date is on a weekend or not.
     * @return True if the day of week of the date is Saturday or Sunday or False otherwise
     */
    public boolean isWeekend(DateTime dateToCheck) {
        int dayToCheck = dateToCheck.getDayOfWeek();
        return WEEKEND_DAYS.contains(dayToCheck);
    }
}
