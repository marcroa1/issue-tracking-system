package com.marcro.issuetracking.datetime;

import static com.marcro.issuetracking.datetime.Days.*;
import static com.marcro.issuetracking.datetime.Months.*;

public class Utils {

    /**
     * Format integers to be the required length, without changing its value.
     * This is achieved by concatenating zeros, as a prefix, to the given number.
     */
    public static String formatLength(int number, int length) {
        StringBuilder zerosBeforeNumber = new StringBuilder();
        int decimalPlacer = 1;
        for (int i = 1; i < length; i++) {
            decimalPlacer *= 10;
            if (number < decimalPlacer) {
                zerosBeforeNumber.append("0");
            }
        }
        return zerosBeforeNumber.toString() + number;
    }

    /**
     * Returns the dooms day of the given year.
     * It uses John Conway's algorithm,
     * to calculate which day of week is the doomsday for the year.
     * Credit to:
     * <li><a href="https://en.wikipedia.org/wiki/Doomsday_rule">Wikipedia</a>
     * <li><a href="https://www.youtube.com/watch?v=714LTMNJy5M">Youtube</a>
     * @param year  The given year to find the doomsday
     * @return The day of the week: 0 as sunday and 6 as saturday
     */
    public static int getDoomsDay(int year) {
        int century = year / 100;
        int anchorDay = getAnchorDay(century);
        int lastTwoDigits = year % 100;
        int a = lastTwoDigits / 12;
        int b = lastTwoDigits % 12;
        return ((a + b + b/4) % 7 + anchorDay) % 7;
    }

    private static int getAnchorDay(int century) {
        int anchorDay = 0;
        switch (century % 4) {
            case 0 -> anchorDay = TUESDAY;
            case 1 -> {/* anchor day remains 0 */}
            case 2 -> anchorDay = FRIDAY;
            case 3 -> anchorDay = WEDNESDAY;
        }
        return anchorDay;
    }

    /**
     * @return The number of days in the given month.
     */
    public static int getDaysInMonth(int month, boolean isLeapYear) {
        if (month == FEBRUARY) {
            return isLeapYear ? 29 : 28;
        }
        if (month <= JULY) {
            return isEven(month) ? 30 : 31;
        } else {
            return isEven(month) ? 31 : 30;
        }
    }

    private static boolean isEven(int month) {
        return month % 2 == 0;
    }
}
