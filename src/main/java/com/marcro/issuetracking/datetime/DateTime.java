package com.marcro.issuetracking.datetime;

import static com.marcro.issuetracking.datetime.Utils.*;

public class DateTime extends Time {
    private static final int A_YEAR = 12;
    public static final int A_DAY = 24;
    public static final int AN_HOUR = 60;
    private static final int[] leapDoomsDays = {4, 29, 14, 4, 9, 6, 11, 8, 5, 10, 7, 12};
    private static final int[] normalDoomsDays = {3, 28, 14, 4, 9, 6, 11, 8, 5, 10, 7, 12};
    private static final String[] daysOfWeek = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
    public static final int FIVE_WEEKS = 35;
    private int year;
    private int month;
    private int day;

    public DateTime(int year, int month, int day, int hour, int minute, String meridiem) {
        super(hour, minute, meridiem);
        this.year = year;
        this.month = month;
        this.day = day;
    }

    /**
     * Makes an exact copy from another DateTime.
     */
    public static DateTime copy(DateTime from) {
        return new DateTime(
                from.getYear(),
                from.getMonth(),
                from.getDay(),
                from.getMeridiemHour(),
                from.getMinute(),
                from.getMeridiem()
        );
    }

    /**
     * Adds years to this date.
     */
    public void addYears(long years) {
        year += years;
    }

    /**
     * Adds months to this date. If adding months flips a year, it is automatically added to this date.
     */
    public void addMonths(long months) {
        addYears(months / A_YEAR);
        int sumMonths = (int) (month + months % A_YEAR);
        if (sumMonths > A_YEAR) {
            addYears(1);
            month = sumMonths - A_YEAR;
        } else {
            month = sumMonths;
        }
    }

    /**
     * Adds days to this date. If adding days flips a month, it is automatically added to this date.
     */
    public void addDays(long days) {
        for (int i = 0; i < days; i++) {
            incrementDay();
        }
    }

    /**
     * Adds exactly one day to this date. If adding days flips a month, it is automatically added to this date.
     */
    public void incrementDay() {
        if (day + 1 > getDaysInMonth(month, isLeapYear())) {
            addMonths(1);
            day = 1;
        } else {
            day++;
        }
    }

    /**
     * Adds hours to this time. If adding hours flips, a day it is automatically added to this date.
     */
    public void addHours(long hours) {
        addDays(hours / A_DAY);
        int sumHours = (int) (hour + hours % A_DAY);
        if (sumHours > A_DAY) {
            incrementDay();
            hour = sumHours - A_DAY;
        } else {
            hour = sumHours;
        }
    }

    /**
     * Adds minutes to this time. If adding minutes flips an hour, it is automatically added to this time.
     */
    public void addMinutes(long minutes) {
        addHours(minutes / AN_HOUR);
        int sumMinutes = (int) (minute + minutes % AN_HOUR);
        if (sumMinutes > AN_HOUR) {
            addHours(1);
            minute = sumMinutes - AN_HOUR;
        } else {
            minute = sumMinutes;
        }
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public Time getTime() {
        return new Time(getMeridiemHour(), minute, getMeridiem());
    }

    /**
     * @return True if this date is a leap year or false otherwise
     */
    public boolean isLeapYear() {
        return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
    }

    /**
     * Return the day of the week from this date.
     * 0 - Sunday
     * 1 - Monday
     * 2 - Tuesday
     * 3 - Wednesday
     * 4 - Thursday
     * 5 - Friday
     * 6 - Saturday
     * @return an integer matched to the day of the week.
     */
    public int getDayOfWeek() {
        int doomsDay = getDoomsDay(year);
        int doomsDayInMonth = getDoomsDayInCurrentMonth();
        int difference = getDifferenceFromCurrentDay(doomsDay, doomsDayInMonth);
        return normalizeToOneWeek(difference);
    }

    private static int normalizeToOneWeek(int positiveDifference) {
        return positiveDifference % 7;
    }

    private int getDifferenceFromCurrentDay(int doomsDay, int doomsDayInMonth) {
        return doomsDay + day - doomsDayInMonth + FIVE_WEEKS;
    }

    private int getDoomsDayInCurrentMonth() {
        return isLeapYear() ? leapDoomsDays[month - 1] : normalDoomsDays[month - 1];
    }

    /**
     * @return only the string value of the date, without time in it.
     */
    public String getDateString() {
        return year +
                "-" +
                Utils.formatLength(month, 2) +
                "-" +
                Utils.formatLength(day, 2) +
                " " +
                daysOfWeek[getDayOfWeek()];
    }

    @Override
    public String toString() {
        return getDateString() +
                " " +
                super.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DateTime dateTime = (DateTime) o;

        if (year != dateTime.year) return false;
        if (month != dateTime.month) return false;
        if (day != dateTime.day) return false;
        if (hour != dateTime.hour) return false;
        return minute == dateTime.minute;
    }

    @Override
    public int hashCode() {
        int result = year;
        result = 31 * result + month;
        result = 31 * result + day;
        result = 31 * result + hour;
        result = 31 * result + minute;
        return result;
    }
}
