package com.marcro.issuetracking.datetime;

public class Time implements Comparable<Time> {
    public static final String PM = "PM";
    public static final String AM = "AM";
    private static final int MERIDIEM = 12;
    protected int hour;
    protected int minute;

    public Time(int hour, int minute, String meridiem) {
        this.hour = meridiem.equalsIgnoreCase(PM) ? hour + MERIDIEM : hour;
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    /**
     * @return The time in minutes. E.g.: 1 hour 10 minutes will return 70 minutes
     */
    public int getTimeInMinutes() {
        return hour * 60 + minute;
    }

    /**
     * @return AM or PM of this time
     */
    public String getMeridiem() {
        return hour > MERIDIEM ? PM : AM;
    }

    /**
     * @return The 12H style hours (hours only)
     */
    public int getMeridiemHour() {
        return hour > MERIDIEM ? hour - MERIDIEM : hour;
    }

    /**
     * Compares a Time object with this time
     * @param that The time to be compared
     * @return True, if this time is before the given time, or false otherwise
     */
    public boolean isBefore(Time that) {
        return this.compareTo(that) < 0;
    }

    /**
     * Compares a Time object with this time
     * @param that The time to be compared
     * @return True, if this time is after, or equals the given time, or false otherwise
     */
    public boolean isAfter(Time that) {
        return this.compareTo(that) >= 0;
    }

    @Override
    public String toString() {
        return Utils.formatLength(getMeridiemHour(), 2) +
                ":" +
                Utils.formatLength(minute, 2) +
                " " +
                getMeridiem();
    }

    @Override
    public int compareTo(Time that) {
        return Integer.compare(
                this.getTimeInMinutes(),
                that.getTimeInMinutes());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Time time = (Time) o;

        if (hour != time.hour) return false;
        return minute == time.minute;
    }

    @Override
    public int hashCode() {
        int result = hour;
        result = 31 * result + minute;
        return result;
    }
}
