package com.marcro.issuetracking.duration;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.marcro.issuetracking.duration.TimeUnit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WorkDurationTest {

    @Test
    public void testDuration() {
        assertEquals("1 DAYS, 2 HOURS, 10 MINUTES", new WorkDuration(610, MINUTES).toString());
        assertEquals("2 HOURS, 10 MINUTES", new WorkDuration(130, MINUTES).toString());
        assertEquals("40 MINUTES", new WorkDuration(40, MINUTES).toString());
    }

    @Test
    public void testDuration_hours() {
        WorkDuration workDuration = new WorkDuration(4, HOURS);
        assertEquals(new TimeHolder(4, HOURS), workDuration.get(HOURS));
    }

    @Test
    public void testDuration_minutes() {
        WorkDuration workDuration = new WorkDuration(20, MINUTES);
        assertEquals(new TimeHolder(20, MINUTES), workDuration.get(MINUTES));
    }

    @Test
    public void testDuration_unitConversion() {
        WorkDuration workDuration = new WorkDuration(16, HOURS);
        assertEquals(new TimeHolder(2, DAYS), workDuration.get(DAYS));
    }

    @Test
    public void testDuration_onlyConvertedUnit() {
        WorkDuration workDuration = new WorkDuration(3600, MINUTES);
        assertEquals(new TimeHolder(7, DAYS), workDuration.get(DAYS));
        assertEquals(new TimeHolder(4, HOURS), workDuration.get(HOURS));
        assertEquals(new TimeHolder(0, MINUTES), workDuration.get(MINUTES));
    }

    @Test
    public void testDuration_canBeExtended() {
        WorkDuration workDuration = new WorkDuration(1, DAYS);
        workDuration.extend(3, HOURS);
        assertEquals(new TimeHolder(1, DAYS), workDuration.get(DAYS));
        assertEquals(new TimeHolder(3, HOURS), workDuration.get(HOURS));
        assertEquals(new TimeHolder(0, MINUTES), workDuration.get(MINUTES));

        workDuration.extend(5, HOURS);
        assertEquals(new TimeHolder(2, DAYS), workDuration.get(DAYS));
        assertEquals(new TimeHolder(0, HOURS), workDuration.get(HOURS));
        assertEquals(new TimeHolder(0, MINUTES), workDuration.get(MINUTES));

        workDuration.extend(10, MINUTES);
        assertEquals(new TimeHolder(2, DAYS), workDuration.get(DAYS));
        assertEquals(new TimeHolder(0, HOURS), workDuration.get(HOURS));
        assertEquals(new TimeHolder(10, MINUTES), workDuration.get(MINUTES));
    }

    @Test
    public void testDuration_retrieveListOfDurations() {
        WorkDuration workDuration = new WorkDuration(1, DAYS);
        workDuration.extend(3, MINUTES);

        List<TimeHolder> expected = new ArrayList<>();
        expected.add(new TimeHolder(1, DAYS));
        expected.add(new TimeHolder(0, HOURS));
        expected.add(new TimeHolder(3, MINUTES));

        assertTrue(expected.containsAll(workDuration.getTimeHolders()));
        assertTrue(workDuration.getTimeHolders().containsAll(expected));
    }

    @Test
    public void testDuration_retrieveListOfDurations_onlyOneDay() {
        WorkDuration workDuration = new WorkDuration(1, DAYS);

        List<TimeHolder> expected = new ArrayList<>();
        expected.add(new TimeHolder(1, DAYS));
        expected.add(new TimeHolder(0, HOURS));
        expected.add(new TimeHolder(0, MINUTES));

        assertTrue(expected.containsAll(workDuration.getTimeHolders()));
        assertTrue(workDuration.getTimeHolders().containsAll(expected));
    }

}
