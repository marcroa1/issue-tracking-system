package com.marcro.issuetracking.datetime;

import org.junit.Test;

import static com.marcro.issuetracking.datetime.Days.*;
import static com.marcro.issuetracking.datetime.Time.AM;
import static com.marcro.issuetracking.datetime.Time.PM;
import static org.junit.Assert.*;

public class DateTimeTest {

    @Test
    public void testDateTime_fields() {
        DateTime dateTime = new DateTime(2023, 8, 12, 9, 30, AM);
        assertEquals(2023, dateTime.getYear());
        assertEquals(8, dateTime.getMonth());
        assertEquals(12, dateTime.getDay());
        assertEquals(9, dateTime.getHour());
        assertEquals(30, dateTime.getMinute());
        assertEquals("2023-08-12 SAT 09:30 AM", dateTime.toString());
    }

    @Test
    public void testDateTimeFields_convertPMTo24H() {
        DateTime dateTime = new DateTime(2023, 8, 12, 2, 5, PM);
        assertEquals(2023, dateTime.getYear());
        assertEquals(8, dateTime.getMonth());
        assertEquals(12, dateTime.getDay());
        assertEquals(14, dateTime.getHour());
        assertEquals(5, dateTime.getMinute());
        assertEquals("2023-08-12 SAT 02:05 PM", dateTime.toString());
    }

    @Test
    public void testTime_isComparable() {
        Time time = new Time(3, 5, AM);
        assertTrue(time.isBefore(new Time(3, 10, AM)));
        assertFalse(time.isAfter(new Time(4, 20, AM)));
    }

    @Test
    public void testTime_compareEqualsTime() {
        Time time = new Time(3, 5, AM);
        assertFalse(time.isBefore(new Time(3, 5, AM)));
        assertTrue(time.isAfter(new Time(3, 5, AM)));
    }
    @Test
    public void testDateTime_isComparableWithTime() {
        DateTime dateTime = new DateTime(2022, 2, 2, 3, 5, AM);
        assertTrue(dateTime.isBefore(new Time(3, 10, AM)));
        assertFalse(dateTime.isAfter(new Time(4, 20, AM)));
    }


    @Test
    public void testDateTime_isLeapYear() {
        assertTrue(new DateTime(2000, 1, 2,3, 4,AM).isLeapYear());
        assertFalse(new DateTime(2001, 1, 2,3, 4,AM).isLeapYear());
    }

    @Test
    public void testDateTime_theDayOfWeek() {
        assertEquals(FRIDAY, new DateTime(2023, 8, 11, 9, 30, AM).getDayOfWeek());
        assertEquals(MONDAY, new DateTime(2025, 2, 3, 9, 30, AM).getDayOfWeek());
        assertEquals(THURSDAY, new DateTime(2018, 5, 31, 9, 30, AM).getDayOfWeek());
        assertEquals(MONDAY, new DateTime(2023, 8, 7, 10, 0, AM).getDayOfWeek());
        assertEquals(TUESDAY, new DateTime(2023, 8, 8, 10, 0, AM).getDayOfWeek());
    }

    @Test
    public void testDateTime_addYears() {
        DateTime dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addYears(1);
        assertEquals(new DateTime(2024, 12, 4, 10, 30, AM), dateTime);
    }

    @Test
    public void testDateTime_addMonths() {
        DateTime dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addMonths(13);
        assertEquals(new DateTime(2025, 1, 4, 10, 30, AM), dateTime);

        dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addMonths(12);
        assertEquals(new DateTime(2024, 12, 4, 10, 30, AM), dateTime);

        dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addMonths(24);
        assertEquals(new DateTime(2025, 12, 4, 10, 30, AM), dateTime);
        dateTime = new DateTime(2023, 8, 4, 10, 30, AM);
        dateTime.addMonths(2);
        assertEquals(new DateTime(2023, 10, 4, 10, 30, AM), dateTime);

    }

    @Test
    public void testDateTime_addDays() {
        DateTime dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addDays(35);
        assertEquals(new DateTime(2024, 1, 8, 10, 30, AM), dateTime);
        dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addDays(365);
        assertEquals(new DateTime(2024, 12, 3, 10, 30, AM), dateTime);
    }

    @Test
    public void testDateTime_addHours() {
        DateTime dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addHours(10);
        assertEquals(new DateTime(2023, 12, 4, 8, 30, PM), dateTime);

        dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addHours(25);
        assertEquals(new DateTime(2023, 12, 5, 11, 30, AM), dateTime);

        dateTime = new DateTime(2023, 12, 4, 8, 0, PM);
        dateTime.addHours(5);
        assertEquals(new DateTime(2023, 12, 5, 1, 0, AM), dateTime);

    }

    @Test
    public void testDateTime_addMinutes() {
        DateTime dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addMinutes(10);
        assertEquals(new DateTime(2023, 12, 4, 10, 40, AM), dateTime);

        dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addMinutes(70);
        assertEquals(new DateTime(2023, 12, 4, 11, 40, AM), dateTime);

        dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        dateTime.addMinutes(40);
        assertEquals(new DateTime(2023, 12, 4, 11, 10, AM), dateTime);
    }

    @Test
    public void testDateTime_getTime() {
        DateTime dateTime = new DateTime(2023, 12, 4, 10, 30, AM);
        assertEquals(new Time(10, 30, AM), dateTime.getTime());
    }

    @Test
    public void testDateTime_hasCopyMethod() {
        DateTime original = new DateTime(2023, 5, 31, 3, 30, PM);
        DateTime replica = DateTime.copy(original);

        assertEquals(original, replica);
    }
}
