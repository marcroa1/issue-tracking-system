package com.marcro.issuetracking.datetime;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static com.marcro.issuetracking.datetime.Days.*;
import static com.marcro.issuetracking.datetime.Months.*;

public class UtilsTest {
    public static final boolean NORMAL_YEAR = false;
    public static final boolean LEAP_YEAR = true;


    @Test
    public void testFormatLength() {
        assertEquals("002", Utils.formatLength(2, 3));
        assertEquals("2", Utils.formatLength(2, 1));
        assertEquals("0010", Utils.formatLength(10, 4));
    }

    @Test
    public void testGetDoomsDay() {
        assertEquals(TUESDAY, Utils.getDoomsDay(2023));
        assertEquals(SATURDAY, Utils.getDoomsDay(2020));
        assertEquals(WEDNESDAY, Utils.getDoomsDay(1990));
        assertEquals(MONDAY, Utils.getDoomsDay(2101));
        assertEquals(THURSDAY, Utils.getDoomsDay(1889));
        assertEquals(FRIDAY, Utils.getDoomsDay(1890));
    }

    @Test
    public void testGetDaysInMonth() {
        assertEquals(31, Utils.getDaysInMonth(JANUARY, NORMAL_YEAR));
        assertEquals(29, Utils.getDaysInMonth(FEBRUARY, LEAP_YEAR));
        assertEquals(28, Utils.getDaysInMonth(FEBRUARY, NORMAL_YEAR));
        assertEquals(31, Utils.getDaysInMonth(MARCH, NORMAL_YEAR));
        assertEquals(30, Utils.getDaysInMonth(APRIL, NORMAL_YEAR));
        assertEquals(31, Utils.getDaysInMonth(MAY, NORMAL_YEAR));
        assertEquals(30, Utils.getDaysInMonth(JUNE, NORMAL_YEAR));
        assertEquals(31, Utils.getDaysInMonth(JULY, NORMAL_YEAR));
        assertEquals(31, Utils.getDaysInMonth(AUGUST, NORMAL_YEAR));
        assertEquals(30, Utils.getDaysInMonth(SEPTEMBER, NORMAL_YEAR));
        assertEquals(31, Utils.getDaysInMonth(OCTOBER, NORMAL_YEAR));
        assertEquals(30, Utils.getDaysInMonth(NOVEMBER, NORMAL_YEAR));
        assertEquals(31, Utils.getDaysInMonth(DECEMBER, NORMAL_YEAR));
    }
}
