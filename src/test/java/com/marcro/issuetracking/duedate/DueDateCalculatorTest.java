package com.marcro.issuetracking.duedate;

import com.marcro.issuetracking.datetime.DateTime;
import com.marcro.issuetracking.datetime.Time;
import com.marcro.issuetracking.duration.WorkDuration;
import com.marcro.issuetracking.exception.NotWorkingDayException;
import com.marcro.issuetracking.exception.OutsideWorkingHoursException;
import org.junit.Before;
import org.junit.Test;

import static com.marcro.issuetracking.datetime.Time.AM;
import static com.marcro.issuetracking.datetime.Time.PM;
import static com.marcro.issuetracking.duration.TimeUnit.*;
import static org.junit.Assert.*;

public class DueDateCalculatorTest {

    DateTimeValidator dateTimeValidator;
    DueDateCalculator dueDateCalculator;

    @Before
    public void setUp() {
        Time workingTimeStart = new Time(9, 0, AM);
        Time workingTimeEnd = new Time(5, 0, PM);
        dateTimeValidator = new DateTimeValidator(workingTimeStart, workingTimeEnd);
        dueDateCalculator = new DueDateCalculatorImpl(dateTimeValidator);
    }

    @Test
    public void testValidator_isInWorkingHours() {
        Time workingTime = new Time(2, 30, PM);
        assertFalse(dateTimeValidator.isOutsideWorkingHours(workingTime));
    }

    @Test
    public void testValidator_isNotInWorkingHours() {
        Time notWorkingTime = new Time(7, 30, AM);
        assertTrue(dateTimeValidator.isOutsideWorkingHours(notWorkingTime));
    }

    @Test
    public void testValidator_isInWorkingHoursWorksWithDateTime() {
        DateTime workingDateTime = new DateTime(2022, 2, 2, 10, 0, AM);
        assertFalse(dateTimeValidator.isOutsideWorkingHours(workingDateTime));
    }

    @Test
    public void testCalculateDueDate_invalidSubmitTime() {
        DateTime submitTime = new DateTime(2023, 8, 11, 7, 30, AM);
        WorkDuration turnaroundTime = new WorkDuration(3, DAYS);
        OutsideWorkingHoursException exception = assertThrows(OutsideWorkingHoursException.class, () -> dueDateCalculator.calculateDueDate(submitTime, turnaroundTime));
        assertEquals("07:30 AM is outside of working hours", exception.getMessage());
    }

    @Test
    public void testCalculateDueDate_invalidSubmitDate() {
        DateTime submitTime = new DateTime(2023, 8, 12, 7, 30, AM);
        WorkDuration turnaroundTime = new WorkDuration(3, DAYS);
        NotWorkingDayException exception = assertThrows(NotWorkingDayException.class, () -> dueDateCalculator.calculateDueDate(submitTime, turnaroundTime));
        assertEquals("2023-08-12 SAT is not a working day", exception.getMessage());
    }

    @Test
    public void testCalculateDuaDate_simple() throws OutsideWorkingHoursException, NotWorkingDayException {
        DateTime submitTime = new DateTime(2023, 8, 7, 9, 0, AM);
        WorkDuration turnaroundTime = new WorkDuration(1, DAYS);
        turnaroundTime.extend(1, HOURS);
        turnaroundTime.extend(1, MINUTES);

        DateTime dueDate = dueDateCalculator.calculateDueDate(submitTime, turnaroundTime);
        assertEquals(new DateTime(2023, 8, 8, 10, 1, AM), dueDate);
    }

    @Test
    public void testCalculateDuaDate_minuteChangesHour() throws OutsideWorkingHoursException, NotWorkingDayException {
        DateTime submitTime = new DateTime(2023, 8, 7, 9, 0, AM);
        WorkDuration turnaroundTime = new WorkDuration(70, MINUTES);

        DateTime dueDate = dueDateCalculator.calculateDueDate(submitTime, turnaroundTime);
        assertEquals(new DateTime(2023, 8, 7, 10, 10, AM), dueDate);
    }

    @Test
    public void testCalculateDuaDate_hourChangesDay() throws OutsideWorkingHoursException, NotWorkingDayException {
        DateTime submitTime = new DateTime(2023, 8, 7, 4, 0, PM);
        WorkDuration turnaroundTime = new WorkDuration(2, HOURS);

        DateTime dueDate = dueDateCalculator.calculateDueDate(submitTime, turnaroundTime);
        assertEquals(new DateTime(2023, 8, 8, 10, 0, AM), dueDate);
    }

    @Test
    public void testCalculateDuaDate_dayChangesMonth() throws OutsideWorkingHoursException, NotWorkingDayException {
        DateTime submitTime = new DateTime(2023, 8, 31, 4, 0, PM);
        WorkDuration turnaroundTime = new WorkDuration(5, DAYS);

        DateTime dueDate = dueDateCalculator.calculateDueDate(submitTime, turnaroundTime);
        assertEquals(new DateTime(2023, 9, 7, 4, 0, PM), dueDate);
    }

    @Test
    public void testCalculateDuaDate_complexDuration() throws OutsideWorkingHoursException, NotWorkingDayException {
        DateTime submitTime = new DateTime(2023, 8, 11, 9, 0, AM);
        WorkDuration turnaroundTime = new WorkDuration(15, DAYS);
        turnaroundTime.extend(10, HOURS);
        turnaroundTime.extend(30, MINUTES);

        DateTime dueDate = dueDateCalculator.calculateDueDate(submitTime, turnaroundTime);
        assertEquals(new DateTime(2023, 9, 4, 11, 30, AM), dueDate);
    }
}
